import * as functions from "firebase-functions";
import {SftpClient} from "./SftpClient";

// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript
export const pull = functions
    .region("europe-west3")
    .https
    .onRequest((request, response) => {
      const config = {
        host: functions.config().sftp.host,
        port: functions.config().sftp.port,
        username: functions.config().sftp.username,
        password: functions.config().sftp.password,
      };
      const client = new SftpClient(config).cli;
      client.then((client) => {
        client.downloadFile(request.body.remote, request.body.local)
            .then(() => {
              functions.logger.info("Download success!", {
                date: String(new Date()),
              });
              client.close(); // remember to close connection after you finish
            }).catch((error) => {
              functions.logger.error("Error response", error);
            });
      }).catch((e) => {
        console.log(e);
      });
      response.send(`Successfully downloaded file ${request.body.remote} to 
                ${request.body.local}!`);
    });
