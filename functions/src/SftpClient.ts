import {Client, ScpClient} from "node-scp";

export type ClientConfig = {
  host: string;
  port: number;
  username?: string;
  password?: string;
  algorithms?: Record<string, unknown>;
}

/**
 * SFTP Client extended from node-scp package
 * https://github.com/maitrungduc1410/node-scp-async
 */
export class SftpClient {
  /**
   * node-scp Client Promise
   */
  public cli: Promise<ScpClient>;
  /**
   * Client Configuration
   * @param {ClientConfig} config
   */
  public constructor(config: ClientConfig) {
    if (!config.algorithms) {
      config.algorithms = {
        // cipher: [
        //   "aes256-ctr",
        // ],
        // serverHostKey: [
        //   "ssh-rsa",
        // ],
        kex: [
          "diffie-hellman-group1-sha1",
          "ecdh-sha2-nistp256",
          "ecdh-sha2-nistp384",
          "ecdh-sha2-nistp521",
          "diffie-hellman-group-exchange-sha256",
          "diffie-hellman-group14-sha1",
        ],
        cipher: [
          "3des-cbc",
          "aes128-ctr",
          "aes192-ctr",
          "aes256-ctr",
        ],
        serverHostKey: [
          "ssh-rsa",
          "ecdsa-sha2-nistp256",
          "ecdsa-sha2-nistp384",
          "ecdsa-sha2-nistp521",
        ],
        hmac: [
          "hmac-sha2-256",
          "hmac-sha2-512",
          "hmac-sha1",
        ],
      };
    }
    // eslint-disable-next-line new-cap
    this.cli = Client(config);
  }
}
