# Zgz SFTP Pull

Download a file from a SFTP host

## Environment variables

The behaviour of the container can be modified with the following environment variables:

| ENV Variable | Content                      | Example |
| ------------ | ---------------------------- | ------- |
| VARIABLE_1   | Tells something to container | "true"  |

## Firebase & emulators

This project contains a docker-friendly implementation for Firebase adapted to run on VS Code Remote Containers.

- Project initialization: _within container shell_
  - `firebase login`
  - `firebase init`:
    - Select project features and emulation capabilities (optional)
    - Select GCP project (or create a new one)
    - Configure selected functionalities
    - Language preferences and linter
    - Install NPM dependencies
- Basic usage:
  - `firebase emulators:start`: launches emulator platform. If run from VS code all ports will be forwarded automatically
  - `firebase deploy`: Deploys everything to GCP. **We strongly recommend using CI/CD instead of this functionality**
- Go to [Connect your app and start prototyping](https://firebase.google.com/docs/emulator-suite/connect_and_prototype) to read the proper documentation

[Firebase CLI Reference](https://firebase.google.com/docs/cli)

### Available emulators

This project can emulate the following parts of GCP:

- Hosting emulator
- Cloud Functions Emulator
- Cloud Pub/Sub Emulator
- Realtime Database Emulator
- Cloud Firestore Emulator
- Auth Emulator
- Cloud Storage Emulator
  **Warning:** If linked to an actual GCP project, **all not emulated resources will run on GCP**.

## Development environment setup

This template is ready for VS Code integration with docker with extension
[Remote - Containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)

- Linux development: If the extension is installed in VS Code and docker in
  the machine, press F1 and look for `Remote-Containers: Open Folder in Container...`
- Windows development: **WSL 2** and docker windows are required.
  **It is important to have it installed this way. The default docker for windows installation is not compatible.**.
  Once installed, press F1 and select `Remote-Containers: Open Folder in Container...` to run the full environment.
  - Install WSL 2 (Requires at least Windows 10 version 1909):
    1. Open a powershell as administrator and run the following command to enable wsl2
       ```powershell
       dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
       ```
    1. Run the following coommnad (also in powershell as administrator) to enable Virtual Machine platform
       ```powershell
       dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
       ```
    1. **Restart your computer**
    1. Update WSL Kernel
       - Use [this link](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi)
         to download the kernel updater that enables WSL 2 functions
       - Otherwhise the next step will throw an error
    1. Set WSL 2 as default (also administrator powershell)
       ```powershell
       wsl --set-default-version 2
       ```
    1. _(Optional)_ You can now use the windows store to install several linux istros
       - [Ubuntu](https://www.microsoft.com/store/productId/9N6SVWS3RX71)
       - [Debian](https://www.microsoft.com/store/productId/9MSVKQC78PK6)
  - **Proper** docker for windows
    - [Install docker For windows](https://docs.docker.com/docker-for-windows/install/):
      Installers' page on [hub.docker.com](https://hub.docker.com/editions/community/docker-ce-desktop-windows/)

## Troubleshooting

##### 1. Run emulators + hot-compiler in a single step

Add next NPM commands at `package.json` file like below:

```
{
  "scripts": {
    "build": "tsc",
    "watch": "tsc -w",
    "serve:watch": "(trap 'kill 0' SIGINT; firebase emulators:start & npm run watch)",
  ...
```

Command `serve:watch` will capture SIGNAL 0 (kill) and apply to the pipelined commands `firebase emulators:start` that will execute the emulators UI and `npm run watch` which is `tsc -w` to compile Typescript source after any change on the code.

## CI/CD

TBD
